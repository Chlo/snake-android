package chloedavid.com.outerspacemanager.cobra;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.Log;

import java.util.Random;

public class SnakeView extends GridView {
    private final String LOG_TAG = SnakeView.class.getName();

    private final int TILE_WALL = 0;
    private final int TILE_SNAKE_HEAD = 1;
    private final int TILE_SNAKE_PART = 2;
    private final int TILE_FOOD = 3;

    Random rand = new Random();

    private int maxX;
    private int minX;
    private int maxY;
    private int minY;

    public SnakeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initSnakeView(context);
    }

    public SnakeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initSnakeView(context);
    }

    private void initSnakeView(Context context){
        Log.i(LOG_TAG, "Init...");
        setFocusable((true));

        Resources r = this.getContext().getResources();

        resetTileList(TILE_FOOD + 1);

        loadTile(TILE_WALL, r.getDrawable(R.drawable.ic_brickwall));
        loadTile(TILE_SNAKE_HEAD, r.getDrawable(R.drawable.snake_head));
        loadTile(TILE_SNAKE_PART, r.getDrawable(R.drawable.snake_part));
        loadTile(TILE_FOOD, r.getDrawable(R.drawable.ic_apple));

    }

    @Override
    protected void updateTiles() {

        this.maxX = mNbTileX - 2;
        this.minX = 1;
        this.maxY = mNbTileY - 2;
        this.minY = 1;

        this.setWall();
    }

    public void setWall(){
        Log.i(LOG_TAG, "Update walls...");
        for (int x = 0; x < mNbTileX; x++){
            setTile(TILE_WALL, x, 0);
            setTile(TILE_WALL, x, mNbTileY - 1);
        }

        for (int y = 0; y < mNbTileY; y++){
            setTile(TILE_WALL, 0, y);
            setTile(TILE_WALL, mNbTileX -1, y);
        }
    }

    public void updateSnake(Snake snake) {
        for(int i = 0; i < snake.getLength(); i++) {
            setTile(i==0?TILE_SNAKE_HEAD:TILE_SNAKE_PART,
                    snake.getPart(i).getX(),
                    snake.getPart(i).getY());
        }
    }

    public void updateFood(Point point) {
        setTile(TILE_FOOD, point.getX(), point.getY());
    }

}
