package chloedavid.com.outerspacemanager.cobra.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;

import org.w3c.dom.Document;

import java.net.URL;

public class Webservice extends Service {

    private static final int NOTIFICATION_CHANNEL_ID = 101;
    private static final String NOTIFICATION_CHANNEL_NAME = "WEB_CHANNEL";

    public void onCreate() {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel = new NotificationChannel(Integer.toString(NOTIFICATION_CHANNEL_ID), NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = (android.app.NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            manager.createNotificationChannel(channel);

            Notification.Builder notificationBuilder = new Notification.Builder(this, Integer.toString(NOTIFICATION_CHANNEL_ID));
            Notification notification = notificationBuilder.build();
            startForeground(NOTIFICATION_CHANNEL_ID, notification);
        }
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {

        LoginTask loginTask = new LoginTask(this);
        try {
            URL url = new URL("http://snake.struct-it.fr/login.php?user=snake&pwd=test");
            loginTask.execute(url);
        } catch (Exception ex){

        }

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void notifyLogin(Boolean success, Document mDocument){

    }

}
