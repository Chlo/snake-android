package chloedavid.com.outerspacemanager.cobra;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class OnTouchListener implements View.OnTouchListener {
    private final String LOG_TAG = OnTouchListener.class.getName();

    private MainActivity mainActivity;

    public OnTouchListener(MainActivity activity) {
        this.mainActivity = activity;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Log.d(LOG_TAG, "X: " + motionEvent.getX());
        Log.d(LOG_TAG, "Y: " + motionEvent.getY());

        this.mainActivity.updateDirection(motionEvent.getX(),
                motionEvent.getY());

        return false;
    }
}
