package chloedavid.com.outerspacemanager.cobra.service;

import android.os.AsyncTask;
import android.util.Log;
import android.webkit.CookieManager;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class LoginTask extends AsyncTask<URL, Void, Boolean> {

    private static final String LOG_TAG = LoginTask.class.getName();
    private static final int MAX_ATTEMPT = 3;

    private Webservice mService;
    private Document mDocument;

    public LoginTask(Webservice service) {
        this.mDocument = null;
        this.mService = service;
    }

    @Override
    protected Boolean doInBackground(URL... urls) {
        Boolean isConnected = false;
        int nbAttempt = 0;
        try {
            do {
                HttpURLConnection connection = (HttpURLConnection) urls[0].openConnection();

                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(10000);
                connection.setReadTimeout(5000);
                connection.connect();

                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    Map<String, List<String>> headerFields = connection.getHeaderFields();
                    List<String> cookiesHeader = headerFields.get("Set-Cookie");

                    if (cookiesHeader != null) {
                        for (String cookie : cookiesHeader) {
                            CookieManager cookieManager = CookieManager.getInstance();
                            cookieManager.setCookie("snake", cookie);
                        }
                    }

                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuffer buffer = new StringBuffer();
                    String inputLine;
                    while ((inputLine = reader.readLine()) != null) {
                        buffer.append(inputLine);
                    }
                    reader.close();

                    Log.d(LOG_TAG, "Buffer: " + buffer.toString());

                    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
                    InputSource inputSource = new InputSource(new StringReader((buffer.toString())));

                    this.mDocument = documentBuilder.parse(inputSource);

                    isConnected = true;

                }

                nbAttempt++;

            } while (!isConnected && nbAttempt < MAX_ATTEMPT);
        } catch (Exception ex) {

        }

        return isConnected;
    }

    protected void onPostExecute(Boolean success) {
        this.mService.notifyLogin(success, this.mDocument);
    }
}
