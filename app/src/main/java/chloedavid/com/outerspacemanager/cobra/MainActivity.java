package chloedavid.com.outerspacemanager.cobra;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.Random;

import chloedavid.com.outerspacemanager.cobra.service.Webservice;

public class MainActivity extends AppCompatActivity {

    private static final int REDRAW_INTERVAL_MS = 250;
    private final String LOG_TAG = MainActivity.class.getName();
    private final int PERMISSION_REQUEST_CODE = 101;

    public static final int DIRECTION_NONE = 0;
    public static final int DIRECTION_DOWN = 1;
    public static final int DIRECTION_LEFT = 2;
    public static final int DIRECTION_RIGHT = 3;
    public static final int DIRECTION_UP = 4;
    private final int POINT_OFFSET = 4;

    private Menu mMenu;
    private Snake mSnake;
    private SnakeView mSnakeView;
    private Point mFood;
    private int mDirection;
    private RedrawHandler mHandler;

    private Boolean mIsStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG_TAG, "creating...");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mSnakeView = findViewById(R.id.snake);

        OnTouchListener listener = new OnTouchListener(this);
        this.mSnakeView.setOnTouchListener(listener);
        this.mHandler = new RedrawHandler(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu, menu);

        this.mMenu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.start:
                if (this.mIsStarted) {
                    Log.i(LOG_TAG, "Stopping...");
                    this.mMenu.getItem(0).setIcon(R.drawable.ic_dragon_solid);
                } else {
                    Log.i(LOG_TAG, "Starting...");
                    this.mMenu.getItem(0).setIcon(R.drawable.ic_hippo_solid);
                    startGame();

                }
                this.mIsStarted = !this.mIsStarted;

                if (this.mIsStarted) {
                    Log.e(LOG_TAG, "isStarted = true");
                } else {
                    Log.e(LOG_TAG, "isStarted = false");
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {

        Log.d(LOG_TAG, "Starting");

        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.WAKE_LOCK, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE
        }, PERMISSION_REQUEST_CODE);

        super.onStart();
    }

    @Override
    public void onStop() {
        Intent intent = new Intent(this, Webservice.class);
        stopService(intent);

        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String permission[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length == 3) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                            grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                            grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                        Log.d(LOG_TAG, "Authorization granted");

                        Intent intent = new Intent(this, Webservice.class);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            startForegroundService(intent);
                        } else {
                            startService(intent);
                        }

                    } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Log.d(LOG_TAG, "WAVE_LOCK authorization only granted");
                    } else if (grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        Log.d(LOG_TAG, "INTERNET authorization only granted");
                    } else {
                        Log.d(LOG_TAG, "ACCESS_NETWORK_STATE authorization only granted");
                    }
                } else {
                    Log.e(LOG_TAG, "Bad authorization response received");
                }
                break;

            default:
                Log.e(LOG_TAG, "Unknown authorization response received : " + requestCode);
                break;
        }
    }

    public void startGame(){
        Log.d(LOG_TAG, "Starting new game...");

        this.initSnake();
        this.initFood();
        this.mDirection = DIRECTION_NONE;

        this.mHandler.setInterval(REDRAW_INTERVAL_MS);
        this.mHandler.request();
    }

    public void initSnake() {
        Random random = new Random();
        this.mSnake = new Snake(POINT_OFFSET + random.nextInt(this.mSnakeView.getNbTileX() - 2 * POINT_OFFSET),POINT_OFFSET + random.nextInt(this.mSnakeView.getNbTileY() - 2 * POINT_OFFSET));
    }

    public void initFood(){
        Random random = new Random();
        Boolean isSnakePosition = false;
        Point food;

        do
        {
            isSnakePosition = false;

            food = new Point(POINT_OFFSET +
                    random.nextInt(this.mSnakeView.getNbTileX() - 2 * POINT_OFFSET),
                    POINT_OFFSET +
                            random.nextInt(this.mSnakeView.getNbTileY()- 2 * POINT_OFFSET));

            for(int i=0; i < this.mSnake.getLength(); i++)
            {
                if(this.mSnake.getPart(i).equals(food))
                {
                    isSnakePosition = true;
                    break;
                }
            }
        }
        while (isSnakePosition);

        this.mFood = food;
    }

    public void update() {
        switch (this.mDirection) {
            case DIRECTION_DOWN:
                if (this.mSnake.isBaby() ||
                        this.mSnake.getPart(0).getY() ==
                                this.mSnake.getPart(1).getY())
                    this.mSnake.setDirection(DIRECTION_DOWN);
                break;

            case DIRECTION_RIGHT:
                if (this.mSnake.isBaby() ||
                        this.mSnake.getPart(0).getX() ==
                                this.mSnake.getPart(1).getX())
                    this.mSnake.setDirection(DIRECTION_RIGHT);
                break;

            case DIRECTION_LEFT:
                if (this.mSnake.isBaby() ||
                        this.mSnake.getPart(0).getX() ==
                                this.mSnake.getPart(1).getX())
                    this.mSnake.setDirection(DIRECTION_LEFT);
                break;

            case DIRECTION_UP:
                if (this.mSnake.isBaby() ||
                        this.mSnake.getPart(0).getY() ==
                                this.mSnake.getPart(1).getY())
                    this.mSnake.setDirection(DIRECTION_UP);
                break;

            case DIRECTION_NONE:
            default:
                break;
        }

        if (this.mSnake.getPart(0).equals(this.mFood)) {
            this.mSnake.Update(true);
            this.initFood();
        } else {
            this.mSnake.Update(false);
        }

        if (this.mSnake.getPart(0).getX() < 1 || this.mSnake.getPart(0).getX() > this.mSnakeView.getNbTileX()-2 || this.mSnake.getPart(0).getY() < 1 || this.mSnake.getPart(0).getY() > this.mSnakeView.getNbTileY()-2 || this.mSnake.isBitting()) {

            this.mIsStarted = false;
            this.mMenu.getItem(0).setIcon(R.drawable.ic_dragon_solid);
            this.mSnakeView.clearTiles();
            this.mSnakeView.invalidate();

        } else if(!this.mIsStarted) {
            //Refresh the canvas
            this.mSnakeView.clearTiles();
            this.mSnakeView.invalidate();
        } else {
            //Refresh the canvas
            this.mSnakeView.clearTiles();
            this.mSnakeView.updateFood(this.mFood);
            this.mSnakeView.updateSnake(this.mSnake);
            this.mSnakeView.invalidate();

            this.mHandler.request();
        }
    }

    public void updateDirection(float rawX, float rawY) {
        double x = this.mSnakeView.getTileX(rawX);
        double y = this.mSnakeView.getTileY(rawY);

        switch(this.mSnake.getDirection()) {
            case DIRECTION_UP:
            case DIRECTION_DOWN:
                if(this.mSnake.getPart(0).getX() > x) {
                    this.mDirection = DIRECTION_LEFT;
                } else {
                    this.mDirection = DIRECTION_RIGHT;
                }
                break;

            case DIRECTION_LEFT:

            case DIRECTION_RIGHT:
                if(this.mSnake.getPart(0).getY() > y) {
                    this.mDirection = DIRECTION_UP;
                } else {
                    this.mDirection = DIRECTION_DOWN;
                }
                break;

            default:
                break;
        }
    }

}
